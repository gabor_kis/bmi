# README #

## Body Mass Index Project ##

Body Mass Index (BMI) estimates the fitness level of the human body compared to the person's
optimal weight. It is very easy to calculate from the height and weight of the person, thus it is a
popular metric to assess health conditions.

