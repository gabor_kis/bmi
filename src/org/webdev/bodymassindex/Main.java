package org.webdev.bodymassindex;

import org.webdev.bodymassindex.controller.BMIController;

/**
 * Bady Mass Index Calculator application
 * @author Gabor Kis *
 */
public class Main {

	public static void main(String[] args) {
		
		printSomeExample();
		
	}	
	
	/**
	 * Some input example to present the working and functions of the API
	 */
	public static void printSomeExample() {
		
		BMIController ctrl;
		
		double[][] metric = {
				{ 180, 50 },
				{ 160, 80 },
				{ 170, 70 },
				{ 175, 8000 },
				{ 0, 80 }
		};
		
		double[][] US = {
				{ 5, 10, 120 },
				{ 5, 20, 140 },
				{ 5, 20, 160 },
				{ 0, 0, 160 },
				{ 6, 10, 0 }
		};
		
		for ( int i = 0 ; i < 5 ; i++ ) {
			ctrl = new BMIController();
			System.out.println("Metric" + (i+1) + "\t" + ctrl.getBmiCategory(metric[i][0], metric[i][1]));
			ctrl = new BMIController();
			System.out.println("US" + (i+1) + "\t" +  ctrl.getBmiCategory(US[i][0], US[i][1], US[i][2]));			
		}
	}

}
