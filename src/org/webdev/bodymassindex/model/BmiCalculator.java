package org.webdev.bodymassindex.model;

import java.util.Map;
import java.util.TreeMap;

import org.webdev.bodymassindex.model.exceptions.IllegalBMIArgumentException;

/** 
 * This class implements the interface, calculates the BMI value and can display 
 * the corresponding weight status according to the calculated value
 * from the height and the weight.
 * @author Gabor Kis
 */
public class BmiCalculator implements BodyMassIndex {
	
	/**
	 * The BMI category table 
	 */
	private static final String[][] bmiTableForAdult = {
			{ "16.0", "Severe Thinness" }, 
			{ "17.0", "Moderate Thinness" },
			{ "18.5", "Mild Thinness" }, 
			{ "25.0", "Normal" },
			{ "30.0", "Overweight" }, 
			{ "35.0", "Obese Class I" },
			{ "40.0", "Obese Class II" }, 
			{ "100.0", "Obese Class III" }
	};
	
	
	/**
	 * Instance variables
	 */
	private Map<Double, String> bmiTableAdult;
	private double height;
	private double weight;
	private double bmiValue;
	
	
	/**
	 * default constructor
	 */
	public BmiCalculator(){
		initialize();
		this.height = 0;
		this.weight = 0;
		bmiValue = 0;
	}
	
	
	/**
	 * getters, setters
	 */
	public double getHeight() {
		return height;
	}

	public double getWeight() {
		return weight;
	}
	
	public double getBmiValue() {
		return bmiValue;
	}
	
	public void setBmiValue(double bmi){
		bmiValue = bmi;
	}

	public Map<Double, String> getBmiTableAdult() {
		return bmiTableAdult;
	}
	
	
	/**
	 * Constructor for Metric units using
	 * @param height - cm
	 * @param weight - kg
	 * @throws IllegalBMIArgumentException
	 */
	public BmiCalculator(double height, double weight) throws IllegalBMIArgumentException {
		checkInputs(height, weight);
		initialize();
		this.height = height / 100; // cm >> m
		this.weight = weight;
		bmiValue = calculateBmi();
	}
	

	/**
	 * Constructor for US units
	 * @param heightFoot - Foot
	 * @param heightInch - Inch
	 * @param weightPound - Pound
	 * @throws IllegalBMIArgumentException
	 */
	public BmiCalculator(double heightFoot, double heightInch, double weightPound) throws IllegalBMIArgumentException {
		checkInputsUS(heightFoot, heightInch, weightPound);
		initialize();
		Converter c = new Converter();
		this.height = c.convertFootAndInchToMeter(heightFoot, heightInch);
		this.weight = c.convertPoundToKg(weightPound);
		bmiValue = calculateBmi();
	}	
	
	
	/**
	 * Initializes a TreeMap to get the categories later
	 * Converts a given string array to TreeMap
	 */
	private void initialize() {
		bmiTableAdult = new TreeMap<Double, String>();
		for (String[] s : bmiTableForAdult) {
			bmiTableAdult.put(Double.parseDouble(s[0]), s[1]);
		}
	}
	
	
	/**
	 * This method for checking the input data when using the metric unit constructor
	 * If parameters are 0 or less than 0 it throws the own exception of the application
	 * @param height
	 * @param weight
	 * @throws IllegalBMIArgumentException
	 */
	private void checkInputs(double height, double weight) throws IllegalBMIArgumentException{
		if (height <= 0 || weight <= 0 ) {
			throw new IllegalBMIArgumentException(height, weight);
		}
	}
	
	
	/**
	 * This method for checking the input data when using the US unit constructor
	 * If parameters are 0 or less than 0 it throws the own exception of the application
	 * @param heightFoot
	 * @param heightInch
	 * @param weightPound
	 * @throws IllegalBMIArgumentException
	 */
	private void checkInputsUS(double heightFoot, double heightInch, double weightPound) throws IllegalBMIArgumentException{
		if ((heightFoot + heightInch) <= 0 || weightPound <= 0 ) {
			throw new IllegalBMIArgumentException(heightFoot, heightInch, weightPound);
		}
	}

	
	/**
	 * This method calculates the BMI value
	 * @return 
	 */
	@Override
	public double calculateBmi() {
		return weight / Math.pow(height, 2);
	}

	
	/**
	 * This method returns a sting which contains the BMI value and 
	 * the category of the weight status
	 * This is the expected return of the API
	 * @return
	 */
	@Override
	public String getCategory() {
		String res = "Wrong bmi value! Please check the inputs!";
		for (Map.Entry<Double, String> entry : bmiTableAdult.entrySet()) {
			if (bmiValue <= entry.getKey()) {
				return "BMI = " + String.format("%.2f", bmiValue) + " - "
						+ entry.getValue();
			}
		}
		return res;
	}

}
