package org.webdev.bodymassindex.model;

/**
 * This is the interface for the BmiCalculator class
 * Two methods are defined 
 * @author Gabor Kis
 * @version 1.0
 */

public interface BodyMassIndex {
	
	/**
	 * Calculating the BMI value
	 * @return the BMI value
	 */
	public double calculateBmi();
	
	/**
	 * Displaying the weight status based on the BMI value
	 * @return the Category string based on the BMI values
	 */
	public String getCategory();
	
}
