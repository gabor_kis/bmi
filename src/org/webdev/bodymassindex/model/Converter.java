package org.webdev.bodymassindex.model;

/**
 * @author Gabor Kis
 * This class is for convert the US units (feet, inch, pound)
 * to metric unit *
 */
public class Converter {
	
	private static final double INCH_TO_METER = 0.0254;
	private static final double POUND_TO_KG = 0.453592;	
	
	public double convertFootAndInchToMeter(double foot, double inch){
		return (foot * 12 + inch) * INCH_TO_METER;
	}
	
	public double convertFootMeter(double foot){
		return (foot * 12) * INCH_TO_METER;
	}
	
	public double convertInchToMeter(double inch){
		return inch * INCH_TO_METER;
	}
	
	public double convertPoundToKg(double pound){
		return pound * POUND_TO_KG;
	}

}
