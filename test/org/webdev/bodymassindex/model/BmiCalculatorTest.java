package org.webdev.bodymassindex.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.webdev.bodymassindex.model.exceptions.IllegalBMIArgumentException;

public class BmiCalculatorTest {

	private BmiCalculator calculator;
	
	@Before
	public void setUp() throws Exception {
		calculator = new BmiCalculator();
	}
	
	public void givenABmiCalculatorWithTwoInputParameters(double height, double weight) throws IllegalBMIArgumentException{
		calculator = new BmiCalculator(height, weight);
	}
	
	public void givenABmiCalculatorWithThreeInputParameters(double heightFoot, double heightInch, double weightPound) throws IllegalBMIArgumentException{
		calculator = new BmiCalculator(heightFoot, heightInch, weightPound);
	}
	
	
	@Test
	public void testBmiCalculatorTwoParametersMetricUnit() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithTwoInputParameters(180, 80);
		final double expectedHeight = 1.8;
		final double expectedWeight = 80.0;
		final double actualHeight = calculator.getHeight();
		final double actualWeight = calculator.getWeight();
		assertThat(expectedHeight, is(actualHeight));
		assertThat(expectedWeight, is(actualWeight));
	}
	
	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorTwoParametersWhenZeroHeightAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithTwoInputParameters(0, 80);
	}

	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorTwoParametersWhenZeroWeightAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithTwoInputParameters(180, 0);
	}
	
	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorTwoParametersWhenNegativeHeightAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithTwoInputParameters(-10, 80);
	}
	
	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorTwoParametersWhenNegativeWeightAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithTwoInputParameters(180, -10);
	}
	
	@Test
	public void testBmiCalculatorThreeParametersForUSUnit() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithThreeInputParameters(10, 10, 25);
		final double expectedHeight = (10 * 12 + 10 ) * 0.0254;
		final double expectedWeight = 25 * 0.453592;
		final double actualHeight = calculator.getHeight();
		final double actualWeight = calculator.getWeight();
		assertThat(expectedHeight, is(actualHeight));
		assertThat(expectedWeight, is(actualWeight));				
	}
	
	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorThreeParametersWhenZeroHeightAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithThreeInputParameters(0, 0, 80);
	}
	
	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorThreeParametersWhenZeroWeightAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithThreeInputParameters(10, 10, 0);
	}
	
	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorThreeParametersWhenNegativeHeightFootAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithThreeInputParameters(-10, 10, 80);
	}
	
	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorThreeParametersWhenNegativeHeightInchAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithThreeInputParameters(10, -10, 80);
	}
	
	@Test(expected = IllegalBMIArgumentException.class)
	public void testBmiCalculatorThreeParametersWhenNegativeWeightAdded() throws IllegalBMIArgumentException {
		givenABmiCalculatorWithThreeInputParameters(10, 10, -80);
	}

	@Test
	public void testCalculateBmi() throws IllegalBMIArgumentException {
		calculator = new BmiCalculator(180, 80);
		final double expectedValue = 80 / Math.pow(1.8, 2);
		final double actualValue = calculator.getBmiValue();
		assertThat(actualValue, is(expectedValue));
	}

	@Test
	public void testGetBmi() {
		calculator = new BmiCalculator();
		double[] bmi = {5.2, 16.6, 18, 22, 29, 33.3, 37.7, 58.98, 1500 };
		String[] categories = { 
				"Severe Thinness", 				
				"Moderate Thinness",
				"Mild Thinness", 
				"Normal",
				"Overweight", 
				"Obese Class I",
				"Obese Class II", 
				"Obese Class III",
				"Wrong bmi value! Please check the inputs!"
		};
		
		for(int i = 0 ; i < 9 ; i++){
			calculator.setBmiValue(bmi[i]);
			String expected = null;
			if( i < 8){
				expected = "BMI = " + String.format("%.2f", bmi[i]) + " - " + categories[i];
			}else{
				expected = categories[i];
			}			
			assertEquals(expected, calculator.getCategory());
		}
		
	}



}