package org.webdev.bodymassindex.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ConverterTest {
	
	Converter converter; // = new Converter();
	
	@Before
	public void setUp() throws Exception{
		converter = new Converter();
	}

	@Test
	public void testConvertFootAndInchToMeter() {
		double actual = converter.convertFootAndInchToMeter(0, 0);
		assertEquals(0.0, actual);
		actual = converter.convertFootAndInchToMeter(1, 1);
		assertEquals(0.3302, actual);
		actual = converter.convertFootAndInchToMeter(32.80839895, 0);
		assertEquals(10.0, actual, 0.001);	//expected, actual, delta
	}

	@Parameters public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				{ 1, 0.453592 },
				{ 5, 2.26796 },
				{ 0, 0 }
		});
	}
	
	@Parameter(value = 0) public double pound;
	@Parameter(value = 1) public double kg;

	@Test
	public void testConvertPoundToKg() {
		double actual = converter.convertPoundToKg(pound);
		assertEquals(kg, actual);
	}

}
